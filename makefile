exe: file.o fonctions.o main.o
	gcc -Wall file.o fonctions.o main.o -lm -o exe

file.o: file.c fonctions.h
	gcc -Wall -c file.c -o file.o

fonctions.o: fonctions.c fonctions.h
	gcc -Wall -c fonctions.c -o fonctions.o

main.o: main.c fonctions.h
	gcc -Wall -c main.c -o main.o

clean:
	rm -f *.o
