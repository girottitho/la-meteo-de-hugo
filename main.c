#include "fonctions.h"
#define OPTSTR "t:p:whmg:x:a:b:d:e:i:o:"

/* Fonction Main */
/* Auteur : Thomas.G */
/* Date : 22/01 */
/* Résumé : Lis les arguments et execute les fonctions en fonction de ces derniers */
/* Entrée(s) : t + mode : indiquant un graphique de température selon un certain mode; p + mode : indiquant un graphique de pression selon un certain mode; w : indiquant un graphique de vent; h : indiquant un graphique de hauteur; m : indiquant un graphique d'humidité; g + argument : indiquant la longitude minimale; x + argument : indiquant la longitude maximale; a + argument : indiquant la latitude minimale; b + argument : indiquant la latitude maximale; d + argument indiquant la date minimale; e + argument indiquant la date maximale; i + argument indiquant le nom du fichier d'entrée; o + argument indiquant le nom du fichier de sortie */
/* Sortie(s) : Un entier indiquant si la fonction s'est correctement déroulée */
int main(int argc, char* argv[]){
    arbre monArbre = NULL;
    int opt;
    int compteur_option = 0;
    int mode=0;
    int xmin=0;
    int xmax=0;
    int ymin=0;
    int ymax=0;
    char* dmin;
    char* dmax;
    char* sortie_input;
    char* sortie_output;
    char option = 0;
    while ((opt = getopt(argc, argv, OPTSTR)) != EOF){
        switch(opt){
            case 'q':
                printf("Veuillez vous réferer au README.md");
                return 0;
            case 't':
                compteur_option++;
                option = 't';
                mode = atoi(optarg);
                break;
            case 'p':
                compteur_option++;
                option = 'p';
                mode = atoi(optarg);
                break;
            case 'w':
                compteur_option++;
                option = 'w';
                break;
            case 'h':
                compteur_option++;
                option = 'h';
                break;
            case 'm':
                compteur_option++;
                option = 'm';
                break;
            case 'g':
                xmin = atoi(optarg);
                break;
            case 'x':
                xmax = atoi(optarg);
                break;
            case 'a':
                ymin = atoi(optarg);
                break;
            case 'b':
                ymax = atoi(optarg);
                break;
            case 'd':
                dmin = optarg;
                break;
            case 'e':
                dmax = optarg;
                break;
            case 'i':
                sortie_input = optarg;
                break;
            case 'o':
                sortie_output = optarg;
                break;
            default:
                printf("Erreur\n");
                return 1;
        }
    }
    monArbre = lectureFichier(xmin, xmax, ymin, ymax, dmin, dmax, option, mode, sortie_input);
    ecritureFichier(sortie_output, monArbre, option, mode);
    return 0;
}