#!/bin/bash
debut=$(date +%s)

#Initialisation des variable pour les options géographiques
fr=0
gu=0
spm=0
an=0
oi=0
art=0
region=0
fichierEntree=0
fichierSortie=0
optI=0
optD=0
optT=0
optP=0
optO=0
optH=0
optW=0
optM=0
mode=0
graph=0
dateMin=0
dateMax=0

#On récupère les options entrées par l'utilisateur
while [ $# -gt 0 ]; do
    if [ "$1" = "--help" ]; then
        echo "Bienvenue dans l'aide de ce programme !"
        echo "Ce programme permet de générer des graphiques en traitant un fichier de données météorologiques au format csv"
        echo ""
        echo "APPEL : "
        echo "La commande d'appel du programme doit être formulée comme ceci (l'ordre des options n'a pas d'importance) : "
        echo "./main.sh [-i Arg] [-o Arg] [-t -p -h -m -w Arg] [-d Arg1 Arg2] [-F -G -S -A -O -Q]"
        echo "[-i Arg] : Indique un fichier d'entrée avec Arg le nom du fichier d'entrée"
        echo "[-o Arg] : Indique un fichier de sortie avec Arg le nom du fichier de sortie sans extention"
        echo "[-t -p -h -m -w Arg] : Indique le type de graphique à générer suivi du type"
        echo "[-t] : Indique que l'on veut un graphique de température"
        echo "[-p] : Indique que l'on veut un graphique de pression"
        echo "[-h] : Indique que l'on veut un graphique d'humidité"
        echo "[-m] : Indique que l'on veut un graphique de vitesse du vent"
        echo "[-w] : Indique que l'on veut un graphique de direction du vent"
        echo "[-d Arg1 Arg2] : Indique une plage de date avec Arg1 la date de début et Arg2 la date de fin"
        echo "[-F -G -S -A -O -Q] : Indique la région à traiter"
        echo "[-F] : Indique que l'on veut la France"
        echo "[-G] : Indique que l'on veut la Guyane"
        echo "[-S] : Indique que l'on veut Saint-Pierre-et-Miquelon"
        echo "[-A] : Indique que l'on veut les Antiilles"
        echo "[-O] : Indique que l'on veut l'Ocean Indien"
        echo "[-Q] : Indique que l'on veut l'Antarctique"
        echo "[-h] : Affiche l'aide"
        echo ""
        echo "COMPORTEMENT : "
        echo "L'option -i suivi du nom du fichier de données est obligatoire"
        echo "Si l'option -o n'est pas précisée alors le fichier aura meteo2023.pdf pour nom, de plus le fichier généré sera au format .pdf peu importe le nom"
        echo "Les options -t, -p, -h, -m, -w sont obligatoires et exclusives, un seul type de graphique peut être généré à la fois"
        echo "Si l'option -t ou -p est précisée alors il faut préciser le mode (1,2 ou 3)"
        echo "Les options -F -G -S -A -O -Q sont exclusives, une seule région peut être traitée à la fois"
        echo "Si une option de région n'est pas précisée alors le graphique sera généré pour le monde entier"
        echo "L'option -d est optionnelle, les dates doivent être au format AAAA-MM-JJThh:mm:ss+00:00"
        echo "Si l'option -d n'est pas précisée alors le graphique sera généré pour toute la période de données"
        exit 0
    fi
    case $1 in
        -F) fr=1
        region=-F;;
        -G) gu=1
        region=-G;;
        -S) spm=1
        region=-S;;
        -A) an=1
        region=-A;;
        -O) oi=1
        region=-O;;
        -Q) art=1
        region=-Q;;
        -i) optI=1
        fichierEntree=$2;;
        -o) optO=1
        fichierSortie=$2;;
        -d) optD=1
        dateMin=$2
        dateMax=$3;;
        -t) optT=1
        graph=-t
        mode=$2;;
        -p) optP=1
        graph=-p
        mode=$2;;
        -h) optH=1
        graph=-h;;
        -w) optW=1
        graph=-w;;
        -m) optM=1
        graph=-m;;
    esac
    shift
done

#On teste si les options entrées sont correctes et on affiche les erreurs si ce n'est pas le cas
if [ $((fr + gu + spm + an + oi + art)) -gt 1 ]; then
    echo "Erreur : Trop d'arguments spécifiés : Les arguments -F, -G, -S, -A, -O, -Q sont exclusifs"
    echo "Utilisez l'option --help pour plus d'informations"
    exit 1
fi

if [ $((optT + optP + optH + optW + optM)) -gt 1 ]; then
    echo "Erreur : Trop d'arguments spécifiés : Les arguments -t, -p, -h, -w, -m sont exclusifs"
    echo "Utilisez l'option --help pour plus d'informations"
    exit 1
fi

if [ $((optT + optP + optH + optW + optM)) -eq 0 ]; then

    echo "Erreur : Trop peu d'arguments spécifiés : Un argument -t, -p, -h, -w, -m est obligatoire"
    echo "Utilisez l'option --help pour plus d'informations"
    exit 1
fi

if [ "$optD" -eq 0 ]; then
    dateMin="2000-01-01T00:00:00+00:00"
    dateMax="2025-01-01T00:00:00+00:00"
fi

if [ $((fr + gu + spm + an + oi + art)) -eq 0 ]; then
    minLatitude=-180
    maxLatitude=180
    minLongitude=-90
    maxLongitude=90
fi

if [ "$optI" -eq 0 ]; then
    echo "Erreur : Il faut spécifier un fichier d'entrée"
    echo "Utilisez l'option --help pour plus d'informations"
    exit 1
elif [ ! -f "$fichierEntree" ]; then
    echo "Erreur : Le fichier d'entrée spécifié n'existe pas"
    echo "Utilisez l'option --help pour plus d'informations"
    exit 1
fi

if [ "$optO" -eq 0 ]; then
    fichierSortie="meteo2023"
fi

case $graph in
    -t) if [ "$mode" -eq 0 ]; then
        echo "Erreur : Il faut spécifier un mode pour le graphique de température"
        echo "Utilisez l'option --help pour plus d'informations"
        exit 1
    fi;;
    -p) if [ "$mode" -eq 0 ]; then
        echo "Erreur : Il faut spécifier un mode pour le graphique de pression"
        echo "Utilisez l'option --help pour plus d'informations"
        exit 1
    fi;;
esac

#Les coordonné des différentes régions sont stockées dans des variables
case $region in   
    -F) echo "Prise en compte des stations en France"
    minLongitude=41
    maxLongitude=52
    minLatitude=-5
    maxLatitude=10;;
    -G) echo "Prise en compte des stations en Guyane"
    minLongitude=2
    maxLongitude=6
    minLatitude=-55
    maxLatitude=-51;;
    -S) echo "Prise en compte des stations à Saint-Pierre-et-Miquelon"
    minLongitude=46
    maxLongitude=48
    minLatitude=-57
    maxLatitude=-56;;
    -A) echo "Prise en compte des stations aux Antilles"
    minLongitude=12
    maxLongitude=20
    minLatitude=-66
    maxLatitude=-58;;
    -O) echo "Prise en compte des stations dans l'Ocean Indien"
    minLongitude=-27
    maxLongitude=-10
    minLatitude=41
    maxLatitude=61;;
    -Q) echo "Prise en compte des stations en Antarctique"
    minLongitude=-90
    maxLongitude=-58
    minLatitude=-180
    maxLatitude=180;;
esac

#On renvoie le pdf d'un graphique de type barre d'erreur, qui varie selon x et y en fonction des valeur entrées de la temperature
temperature(){    
    echo "
    set terminal pdf
    set output '$fichierSortie.pdf'
    set grid
    set autoscale x
    set autoscale y
    set xlabel 'Numero de Station'
    set ylabel 'Temperature'
    set nokey
    set sample 20
    set style histogram errorbars
    set multiplot
    set size 1,1
    set title 'Temperature en fonction du numero de la station'
    set origin 0.0,0.0
    plot '$fichierSortie' u 1:2:3:4 with yerrorbars
    unset multiplot" >> graphtest
    gnuplot --persist graphtest
    rm graphtest
}

#On renvoie le pdf d'un graphique de type barre d'erreur, qui varie selon x et y en fonction des valeur entrées de la pression
pression(){
    echo "
    set terminal pdf
    set output '$fichierSortie.pdf'
    set grid
    set autoscale x
    set autoscale y
    set xlabel 'Numero de Station'
    set ylabel 'Pression'
    set nokey
    set sample 20
    set style histogram errorbars
    set multiplot
    set size 1,1
    set title 'Pression en fonction du numero de la station'
    set origin 0.0,0.0
    plot '$fichierSortie' u 1:2:3:4 with yerrorbars
    unset multiplot" >> graphtest
    gnuplot --persist graphtest
    rm graphtest
}

#On renvoie le pdf d'un graphique de type ligne, qui varie selon x et y en fonction des valeurs entrées de temperature
temperature2(){
    echo "
    set terminal pdf
    set output '$fichierSortie.pdf'
    set grid
    set xdata time
    set timefmt '%Y-%m-%dT%H:%M:%S'
    set autoscale x
    set autoscale y
    set xlabel 'Date'
    set ylabel 'Temperature'
    set nokey
    set sample 100
    set multiplot
    set size 1,1
    set title 'Température en fonction de la Date'
    set origin 0.0,0.0
    plot '$fichierSortie' u 1:2 title 'temp2' with line 
    unset multiplot" >> graphtest
    gnuplot --persist graphtest
    rm graphtest
}

#On renvoie le pdf d'un graphique de type ligne, qui varie selon x et y en fonction des valeurs entrées de la pression
pression2(){
    echo "
    set terminal pdf
    set output '$fichierSortie.pdf'
    set grid
    set xdata time
    set timefmt '%Y-%m-%d\T%H:%M:%S'
    set autoscale x
    set autoscale y
    set xlabel 'Date'
    set ylabel 'Pression'
    set nokey
    set sample 20
    set multiplot
    set size 1,1
    set title 'Pression en fonction de la Date'
    set origin 0.0,0.0
    plot '$fichierSortie' u 1:2 title 'temp2' with line 
    unset multiplot" >> graphtest
    gnuplot --persist graphtest
    rm graphtest
}

#On renvoie le pdf d'un graphique de type carte colorée en 3d, qui varie selon x, y et z en fonction des valeurs entrées de la hauteur
hauteur(){
    echo "
    set terminal pdf
    set output '$fichierSortie.pdf'
    set grid
    set autoscale x
    set autoscale y
    set autoscale z
    set xlabel 'Latitude'
    set ylabel 'Longitude'
    set zlabel 'Hauteur'
    set multiplot
    set nokey
    set palette defined (0 'blue', 200 'green', 500 'yellow', 800 'red')
    set xtics offset 0,-0.2
    set ytics offset 0,0
    set dgrid3d 75,75,75
    set border 255
    set title 'Carte topographique de la zone étudiée'
    set xlabel rotate by -8
    set ylabel rotate parallel
    set zlabel rotate by 90
    splot '$fichierSortie' u 2:1:3 with pm3d
    unset multiplot" >> graphtest
    gnuplot --persist graphtest
    rm graphtest
}

#On renvoie le pdf d'un graphique de type carte colorée en 3d, qui varie selon x, y et z en fonction des valeurs entrées de l'humidité
humidite(){
    echo "
    set terminal pdf
    set output '$fichierSortie.pdf'
    set grid
    set autoscale x
    set autoscale y
    set autoscale z
    set xlabel 'Latitude'
    set ylabel 'Longitude'
    set zlabel 'Humidite'
    set multiplot
    set nokey
    set palette defined (0 'blue', 200 'green', 500 'yellow', 800 'red')
    set xtics offset 0,-0.2
    set ytics offset 0,0
    set dgrid3d
    set border 255
    set title 'Carte humidité de la zone étudiée'
    set xlabel rotate by -8
    set ylabel rotate parallel
    set zlabel rotate by 90
    splot '$fichierSortie' u 2:1:3 with pm3d
    unset multiplot" >> graphtest
    gnuplot --persist graphtest
    rm graphtest
}

#Non operationnel le tri n'est pas codé
temperature3(){
    echo "
    set grid
    set xdata time
    set timefmt '%Y-%m-%d\T%H:%M:%S'
    set autoscale x
    set autoscale y
    set xlabel 'temps'
    set ylabel 'temperature'
    set key box
    set sample 20
    set multiplot
    set size 1,1
    set origin 0.0,0.0
    plot for [i = 1:3] '$fichierSortie' u 2*i-1:2*i smooth unique with line 
    unset multiplot" >> graphtest
    gnuplot --persist graphtest
    rm graphtest
}

#Non operationnel le tri n'est pas codé
pression3(){
    echo "
    set grid
    set xdata time
    set timefmt '%Y-%m-%d\T%H:%M:%S'
    set autoscale x
    set autoscale y
    set xlabel 'temps'
    set ylabel 'pression'
    set key box
    set sample 20
    set multiplot
    set size 1,1
    set origin 0.0,0.0
    plot for [i = 1:3] '$fichierSortie' u 2*i-1:2*i smooth unique with line 
    unset multiplot" >> graphtest
    gnuplot --persist graphtest
    rm graphtest
}

#On renvoie le pdf d'un graphique de type vecteur, qui varie selon x et y en fonction des valeurs entrées du vent
vent(){
    echo "
    set terminal pdf
    set output '$fichierSortie.pdf'
    set grid
    set autoscale x
    set autoscale y
    set xlabel 'Longitude'
    set ylabel 'Latitude'
    set nokey
    set multiplot
    set size 1,1
    set origin 0.0,0.0
    set title 'Carte des vents de la zone étudiée'
    plot '$fichierSortie' using 2:1:3:4 with vectors head filled lc rgb 'red' notitle
    unset multiplot" >> graphtest
    gnuplot --persist graphtest
    rm graphtest
}

#On compile le programme
echo "Compilation du programme..."   
make >> /dev/null
make clean >> /dev/null

#On vérifie que le programme a bien été compilé
if [ ! -f "exe" ]; then
    echo "Erreur de compilation : Vérifiez que tous les fichier .c et .h sont bien présents dans le dossier"
    exit 1
fi

#Les différentes options qui peuvent être utilisés par l'utilisateur
case $graph in 
    -t) if [ "$mode" -eq 1 ]; then
        echo "Lecture des données et conversion..."
        ./exe -i "$fichierEntree" -o "$fichierSortie" -d "$dateMin" -e "$dateMax" -g "$minLongitude" -x "$maxLongitude" -a "$minLatitude" -b "$maxLatitude" "$graph" "$mode"
        echo "Export en Image..."
        temperature
    fi
    if [ "$mode" -eq 2 ]; then
        echo "Lecture des données et conversion..."
        ./exe -i "$fichierEntree" -o "$fichierSortie" -d "$dateMin" -e "$dateMax" -g "$minLongitude" -x "$maxLongitude" -a "$minLatitude" -b "$maxLatitude" "$graph" "$mode"
        echo "Export en Image..."
        temperature2
    fi;;
    -p) if [ "$mode" -eq 1 ]; then
        echo "Lecture des données et conversion..."
        ./exe -i "$fichierEntree" -o "$fichierSortie" -d "$dateMin" -e "$dateMax" -g "$minLongitude" -x "$maxLongitude" -a "$minLatitude" -b "$maxLatitude" "$graph" "$mode"
        echo "Export en Image..."
        pression
    fi
    if [ "$mode" -eq 2 ]; then
        echo "Lecture des données et conversion..."
        ./exe -i "$fichierEntree" -o "$fichierSortie" -d "$dateMin" -e "$dateMax" -g "$minLongitude" -x "$maxLongitude" -a "$minLatitude" -b "$maxLatitude" "$graph" "$mode"
        echo "Export en Image..."
        pression2
    fi;;
    -h) echo "Lecture des données et conversion..."
    ./exe -i "$fichierEntree" -o "$fichierSortie" -d "$dateMin" -e "$dateMax" -g "$minLongitude" -x "$maxLongitude" -a "$minLatitude" -b "$maxLatitude" "$graph" "$mode"
        echo "Export en Image..."
    hauteur;;
    -w) echo "Lecture des données et conversion..."
    ./exe -i "$fichierEntree" -o "$fichierSortie" -d "$dateMin" -e "$dateMax" -g "$minLongitude" -x "$maxLongitude" -a "$minLatitude" -b "$maxLatitude" "$graph" "$mode"
        echo "Export en Image..."
    vent;;
    -m) echo "Lecture des données et conversion..."
    ./exe -i "$fichierEntree" -o "$fichierSortie" -d "$dateMin" -e "$dateMax" -g "$minLongitude" -x "$maxLongitude" -a "$minLatitude" -b "$maxLatitude" "$graph" "$mode"
        echo "Export en Image..."
    humidite;;
esac
echo "Génération du graphique terminé !"

#On supprime les fichiers qu'on a créé
rm -f exe
rm -f "$fichierSortie" 

#On écrit le temps d'exécution du programme
fin=$(date +%s)
echo "Temps d'exécution : $((fin-debut)) secondes"
