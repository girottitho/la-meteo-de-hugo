#include "fonctions.h"

arbre lectureFichier(int xmin, int xmax, int ymin, int ymax, char* dateMinTemp, char* dateMaxTemp, char option, int mode, char* nomFichier){
    arbre monArbre = NULL;
    dates* dateMin = malloc(sizeof(dates));
    dates* dateMax = malloc(sizeof(dates));
    sscanf(dateMinTemp, "%d-%d-%dT%d:%d:%d+%d:%d", &dateMin->annee, &dateMin->mois, &dateMin->jour, &dateMin->heure, &dateMin->minute, &dateMin->seconde, &dateMin->decalage_heures, &dateMin->decalage_minutes);dateMin->heure+=dateMin->decalage_heures;dateMin->dateEnHeure = dateMin->annee*8928 + dateMin->mois*744 + dateMin->jour*24 + dateMin->heure;
    sscanf(dateMaxTemp, "%d-%d-%dT%d:%d:%d+%d:%d", &dateMax->annee, &dateMax->mois, &dateMax->jour, &dateMax->heure, &dateMax->minute, &dateMax->seconde, &dateMax->decalage_heures, &dateMax->decalage_minutes);dateMax->heure+=dateMax->decalage_heures;dateMax->dateEnHeure = dateMax->annee*8928 + dateMax->mois*744 + dateMax->jour*24 + dateMax->heure;
    FILE* file;
    char tampon[512];
    char* colonnes[15];
    char* temp;
    file = fopen(nomFichier, "r");
    if (file == NULL){
        printf("Erreur d'ouverture du fichier !\n");
        exit(1);
    } else {
        fgets(tampon, 512, file);
    }
    while (((temp = fgets(tampon, 512, file)) != NULL) && strcmp(temp,"\n")){
        memset(colonnes, 0, sizeof(colonnes));
        int j = 0;
        int memoireDebut = 0;
        for (int i = 0; i < strlen(tampon); i++){
            if (tampon[i] == ';' || tampon[i] == '\n' || tampon[i] == EOF){
                int taille = i - memoireDebut;
                colonnes[j] = malloc(taille + 1);
                memcpy(colonnes[j], &tampon[memoireDebut], taille);
                colonnes[j][taille] = '\0';
                j++;
                memoireDebut = i + 1;
            }
        }
        for (int u = 0; u < 15; u++){
            if(strcmp(colonnes[u], "") == 0){
                strcpy(colonnes[u], "-9999");
            }
        }
        char* coordonnesTemp = malloc(strlen(colonnes[9])+1*sizeof(char));
        strcpy(coordonnesTemp,colonnes[9]);
        float xvalue = 0;
        float yvalue = 0;
        sscanf(coordonnesTemp, "%f,%f", &xvalue, &yvalue);
        dates* dateActuelle = malloc(sizeof(dates));
        sscanf(colonnes[1], "%d-%d-%dT%d:%d:%d+%d:%d", &dateActuelle->annee, &dateActuelle->mois, &dateActuelle->jour, &dateActuelle->heure, &dateActuelle->minute, &dateActuelle->seconde, &dateActuelle->decalage_heures, &dateActuelle->decalage_minutes);dateActuelle->heure+=dateActuelle->decalage_heures;dateActuelle->dateEnHeure = dateActuelle->annee*8928 + dateActuelle->mois*744 + dateActuelle->jour*24 + dateActuelle->heure;
        if((xvalue >= xmin) && (xvalue <= xmax) && (yvalue >= ymin) && (yvalue <= ymax)){
            if((dateActuelle->dateEnHeure >= dateMin->dateEnHeure) && (dateActuelle->dateEnHeure <= dateMax->dateEnHeure)){
                monArbre = ajouterNoeud(monArbre, atoi(colonnes[0]), *dateActuelle, atoi(colonnes[2]), atof(colonnes[3]), atof(colonnes[4]), atoi(colonnes[5]), colonnes[9], atoi(colonnes[10]), atoi(colonnes[13]), option, mode);
            }
        }
    }
    fclose(file);
    return monArbre;
}

int ecritureTemp_PressMode1(FILE* fichier, arbre monArbre){
    if(monArbre != NULL){
        ecritureTemp_PressMode1(fichier, monArbre->gauche);
        fprintf(fichier, "%d %f %d %d\n", monArbre->gg.ID_station, (monArbre->gg.temp_press/(float)monArbre->gg.compteur), (int)monArbre->gg.temp_press_min, (int)monArbre->gg.temp_press_max);
        ecritureTemp_PressMode1(fichier, monArbre->droit);
    }
    return 0;
}

int ecritureTemp_PressMode2(FILE* fichier, arbre monArbre){
    if(monArbre != NULL){
        ecritureTemp_PressMode2(fichier, monArbre->gauche);
        fprintf(fichier, "2010-01-%dT%d:00:00+00:00 %d\n", monArbre->gg.dateActuelle.jour, monArbre->gg.dateActuelle.heure, ((int)monArbre->gg.temp_press/(int)monArbre->gg.compteur));
        ecritureTemp_PressMode2(fichier, monArbre->droit);
    }
    return 0;
}

int ecritureVent(FILE* fichier, arbre monArbre){
    if(monArbre != NULL){
        ecritureVent(fichier, monArbre->gauche);
        fprintf(fichier, "%f %f %f %f\n", monArbre->gg.latitude, monArbre->gg.longitude, (monArbre->gg.x_vent/(float)monArbre->gg.compteur), (monArbre->gg.y_vent/(float)monArbre->gg.compteur));
        ecritureVent(fichier, monArbre->droit);
    }
    return 0;
}

int ecritureHauteur_Humidite(FILE* fichier, arbre monArbre){
    if(monArbre != NULL){
        ecritureHauteur_Humidite(fichier, monArbre->gauche);
        fprintf(fichier, "%f %f %d\n", monArbre->gg.latitude, monArbre->gg.longitude, monArbre->gg.Hauteur_Humidite);
        ecritureHauteur_Humidite(fichier, monArbre->droit);
    }
    return 0;
}

int ecritureFichier(char* nomFichier , arbre monArbre, char option, int mode){
    FILE* file;
    file = fopen(nomFichier, "w");
    switch (option){
    case 't':
        if (mode == 1){
            ecritureTemp_PressMode1(file, monArbre);
        } else if (mode == 2){
            ecritureTemp_PressMode2(file, monArbre);
        } else if (mode == 3){
            
        }
        break;
    case 'p':
        if (mode == 1){
            ecritureTemp_PressMode1(file, monArbre);
        } else if (mode == 2){
            ecritureTemp_PressMode2(file, monArbre);
        } else if (mode == 3){
            
        }
        break;
    case 'w':
        ecritureVent(file, monArbre);
        break;
    case 'h':
        ecritureHauteur_Humidite(file, monArbre);
        break;
    case 'm':
        ecritureHauteur_Humidite(file, monArbre);
        break;
    default:
        printf("Erreur d'option\n");
        break;
    }
    fclose(file);
    return 0;
}