#ifndef __METEO_H__
#define __METEO_H__
#define _USE_MATH_DEFINES

//Inclusion des librairies nécessaires
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <getopt.h>

//Définition des macros
#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

//Prototypage de la structure de stockage des dates 
typedef struct dates{
    int annee;
    int mois;
    int jour;
    int heure;
    int minute;
    int seconde;
    int decalage_heures;
    int decalage_minutes;
    int dateEnHeure;
}dates;

//Prototype de la structure de stockage des mesures
typedef struct mesures{
    int ID_station;
    dates dateActuelle;
    float x_vent;
    float y_vent;
    int Hauteur_Humidite;
    float longitude;
    float latitude;
    float temp_press;
    float temp_press_min;
    float temp_press_max;
    int compteur;
}mesures;

//Prototypage de la structure de l'arbre et du noeud
typedef struct noeud{
    mesures gg;
    struct noeud* gauche;
    struct noeud* droit;
}noeud;
typedef noeud* arbre;

/* Fonction Lecture du Fichier */
/* Auteur : Thomas.G */
/* Date : 22/01 */
/* Résumé : Lis le fichier et sépare les différentes données dans le csv séparés par des ; */
/* Entrée(s) : int xmin, int xmax, int ymin, int ymax, char* dateMin, char* dateMax, char option, int mode, char* nomFichier */
/* Sortie(s) : Un arbre contenant les valeurs utiles lues */
arbre lectureFichier(int xmin, int xmax, int ymin, int ymax, char* dateMin, char* dateMax, char option, int mode, char* nomFichier);

/* Fonction Ecriture du Fichier */
/* Auteur : Thomas.G */
/* Date : 22/01 */
/* Résumé : Ecrit décide quelle fonction d'écriture spécifique appeler */
/* Entrée(s) : char* nomFichier, arbre monArbre, char option, int mode */
/* Sortie(s) : Un entier indiquant si l'écriture s'est bien passée */
int ecritureFichier(char* nomFichier , arbre monArbre, char option, int mode);

/* Fonction Ajout de Noeud */
/* Auteur : Thomas.G */
/* Date : 22/01 */
/* Résumé : Ajoute un noeud à l'arbre contenant les valeurs utiles */
/* Entrée(s) : arbre monArbre, int ID_station, dates dateActuelle, int pression, float angleVent, float normeVent, int humidite, char* coordonnees, int temperature, int hauteur, char option, int mode */
/* Sortie(s) : Un arbre contenant les valeurs utiles */
arbre ajouterNoeud(arbre monArbre, int ID_station, dates dateActuelle, int pression, float angleVent, float normeVent, int humidite, char* coordonnees, int temperature, int hauteur, char option, int mode);

/* Fonction Insertion pour Temperature et Pression en mode 1 */
/* Auteur : Thomas.G */
/* Date : 22/01 */
/* Résumé : Insère les valeurs de température et de pression dans l'arbre */
/* Entrée(s) : arbre monArbre, arbre arbreTemp */
/* Sortie(s) : Un arbre avec le noeud voulu ajouté */
arbre insertionTemp_PressMode1(arbre monArbre, arbre arbreTemp);

/* Fonction Ecriture pour Temperature et Pression en mode 1 */
/* Auteur : Thomas.G */
/* Date : 22/01 */
/* Résumé : Ecrit les valeurs de température et de pression dans le fichier */
/* Entrée(s) : FILE* fichier, arbre monArbre */
/* Sortie(s) : Un entier indiquant si l'écriture s'est bien passée */
int ecritureTemp_PressMode1(FILE* fichier, arbre monArbre);

/* Fonction Insertion pour Temperature et Pression en mode 2 */
/* Auteur : Thomas.G */
/* Date : 22/01 */
/* Résumé : Insère les valeurs de température et de pression dans l'arbre */
/* Entrée(s) : arbre monArbre, arbre arbreTemp */
/* Sortie(s) : Un arbre avec le noeud voulu ajouté */
arbre insertionTemp_PressMode2(arbre monArbre, arbre arbreTemp);

/* Fonction Ecriture pour Temperature et Pression en mode 2 */
/* Auteur : Thomas.G */
/* Date : 22/01 */
/* Résumé : Ecrit les valeurs de température et de pression dans le fichier */
/* Entrée(s) : FILE* fichier, arbre monArbre */
/* Sortie(s) : Un entier indiquant si l'écriture s'est bien passée */
int ecritureTemp_PressMode2(FILE* fichier, arbre monArbre);

/* Fonction Insertion pour Vent */
/* Auteur : Thomas.G */
/* Date : 22/01 */
/* Résumé : Insère les valeurs de vent dans l'arbre */
/* Entrée(s) : arbre monArbre, arbre arbreTemp */
/* Sortie(s) : Un arbre avec le noeud voulu ajouté */
arbre insertionVent(arbre monArbre, arbre arbreTemp);

/* Fonction Ecriture pour Vent */
/* Auteur : Thomas.G */
/* Date : 22/01 */
/* Résumé : Ecrit les valeurs de vent dans le fichier */
/* Entrée(s) : FILE* fichier, arbre monArbre */
/* Sortie(s) : Un entier indiquant si l'écriture s'est bien passée */
int ecritureVent(FILE* fichier, arbre monArbre);

/* Fonction Insertion pour Hauteur et Humidite */
/* Auteur : Thomas.G */
/* Date : 22/01 */
/* Résumé : Insère les valeurs de hauteur et d'humidité dans l'arbre */
/* Entrée(s) : arbre monArbre, arbre arbreTemp */
/* Sortie(s) : Un arbre avec le noeud voulu ajouté */
arbre insertionHauteur_Humidite(arbre monArbre, arbre arbreTemp);

/* Fonction Ecriture pour Hauteur et Humidite */
/* Auteur : Thomas.G */
/* Date : 22/01 */
/* Résumé : Ecrit les valeurs de hauteur et d'humidité dans le fichier */
/* Entrée(s) : FILE* fichier, arbre monArbre */
/* Sortie(s) : Un entier indiquant si l'écriture s'est bien passée */
int ecritureHauteur_Humidite(FILE* fichier, arbre monArbre);

#endif