#include "fonctions.h"

arbre ajouterNoeud(arbre monArbre, int ID_station, dates dateActuelle, int pression, float angleVent, float normeVent, int humidite, char* coordonnees, int temperature, int hauteur, char option, int mode){
    arbre arbreTemp = malloc(1*sizeof(noeud));
    arbreTemp->gg.ID_station = ID_station;
    sscanf(coordonnees, "%f,%f", &arbreTemp->gg.latitude, &arbreTemp->gg.longitude);
    switch (option){
        case 't':
            if(temperature != -9999){
                arbreTemp->gg.temp_press = temperature;
                if(mode == 1){
                    monArbre = insertionTemp_PressMode1(monArbre, arbreTemp);
                }else if(mode == 2){
                    arbreTemp->gg.dateActuelle = dateActuelle;
                    monArbre = insertionTemp_PressMode2(monArbre, arbreTemp);
                }
            }
            break;
        case 'p':
            if(pression != -9999){
                arbreTemp->gg.temp_press = pression;
                if(mode == 1){
                    monArbre = insertionTemp_PressMode1(monArbre, arbreTemp);
                } else if(mode == 2){
                    arbreTemp->gg.dateActuelle = dateActuelle;
                    monArbre = insertionTemp_PressMode2(monArbre, arbreTemp);
                }
            }
            break;
        case 'w':
            if((normeVent != -9999) && (angleVent != -9999)){
                arbreTemp->gg.x_vent = normeVent * (cosf(angleVent * M_PI / 180));
                arbreTemp->gg.y_vent = normeVent * (sinf(angleVent * M_PI / 180));
                monArbre = insertionVent(monArbre, arbreTemp);
            }
            break;
        case 'h':
            if(hauteur != -9999){
                arbreTemp->gg.Hauteur_Humidite = hauteur;
                monArbre = insertionHauteur_Humidite(monArbre, arbreTemp);
            }
            break;
        case 'm':
            if(humidite != -9999){
                arbreTemp->gg.Hauteur_Humidite = humidite;
                monArbre = insertionHauteur_Humidite(monArbre, arbreTemp);
            }
            break;
        default:
            break;
    }
    return monArbre;
}

arbre insertionTemp_PressMode1(arbre monArbre, arbre arbreTemp){
    if(monArbre != NULL){
        if(monArbre->gg.ID_station == arbreTemp->gg.ID_station){
            monArbre->gg.temp_press += arbreTemp->gg.temp_press;
            monArbre->gg.temp_press_min = min(monArbre->gg.temp_press_min, arbreTemp->gg.temp_press);
            monArbre->gg.temp_press_max = max(monArbre->gg.temp_press_max, arbreTemp->gg.temp_press);
            monArbre->gg.compteur++;
        } else {
            if(monArbre->gg.ID_station > arbreTemp->gg.ID_station){
                monArbre->gauche = insertionTemp_PressMode1(monArbre->gauche, arbreTemp);
            } else {
                monArbre->droit = insertionTemp_PressMode1(monArbre->droit, arbreTemp);
            }
        }
    } else {
        monArbre = arbreTemp;
        monArbre->gg.temp_press_min = arbreTemp->gg.temp_press;
        monArbre->gg.temp_press_max = arbreTemp->gg.temp_press;
    }
    return monArbre;
}

arbre insertionTemp_PressMode2(arbre monArbre, arbre arbreTemp){
    if(monArbre != NULL){
        if(monArbre->gg.dateActuelle.jour*24 + monArbre->gg.dateActuelle.heure == arbreTemp->gg.dateActuelle.jour*24 + arbreTemp->gg.dateActuelle.heure){
            monArbre->gg.temp_press += arbreTemp->gg.temp_press;
            monArbre->gg.dateActuelle.jour = arbreTemp->gg.dateActuelle.jour;
            monArbre->gg.dateActuelle.heure = arbreTemp->gg.dateActuelle.heure;
            monArbre->gg.dateActuelle.mois = arbreTemp->gg.dateActuelle.mois;
            monArbre->gg.dateActuelle.annee = arbreTemp->gg.dateActuelle.annee;
            monArbre->gg.dateActuelle.decalage_heures = arbreTemp->gg.dateActuelle.decalage_heures;
            monArbre->gg.compteur++;
        } else {
            if(monArbre->gg.dateActuelle.jour*24 + monArbre->gg.dateActuelle.heure > arbreTemp->gg.dateActuelle.jour*24 + arbreTemp->gg.dateActuelle.heure){
                monArbre->gauche = insertionTemp_PressMode2(monArbre->gauche, arbreTemp);
            } else {
                monArbre->droit = insertionTemp_PressMode2(monArbre->droit, arbreTemp);
            }
        }
    } else {
        monArbre = arbreTemp;
        monArbre->gg.temp_press += arbreTemp->gg.temp_press;
        monArbre->gg.dateActuelle.jour = arbreTemp->gg.dateActuelle.jour;
        monArbre->gg.dateActuelle.heure = arbreTemp->gg.dateActuelle.heure;
        monArbre->gg.dateActuelle.mois = arbreTemp->gg.dateActuelle.mois;
        monArbre->gg.dateActuelle.annee = arbreTemp->gg.dateActuelle.annee;
        monArbre->gg.dateActuelle.decalage_heures = arbreTemp->gg.dateActuelle.decalage_heures;
        monArbre->gg.compteur++;
    }
    return monArbre;
}

arbre insertionVent(arbre monArbre, arbre arbreTemp){
    if(monArbre != NULL){
        if(monArbre->gg.ID_station == arbreTemp->gg.ID_station){
            monArbre->gg.x_vent += arbreTemp->gg.x_vent;
            monArbre->gg.y_vent += arbreTemp->gg.y_vent;
            monArbre->gg.compteur++;
        } else {
            if(monArbre->gg.ID_station > arbreTemp->gg.ID_station){
                monArbre->gauche = insertionVent(monArbre->gauche, arbreTemp);
            } else {
                monArbre->droit = insertionVent(monArbre->droit, arbreTemp);
            }
        }
    } else {
        monArbre = arbreTemp;
    }
    return monArbre;
}

arbre insertionHauteur_Humidite(arbre monArbre, arbre arbreTemp){
    if(monArbre != NULL){
        if(monArbre->gg.ID_station == arbreTemp->gg.ID_station){
            monArbre->gg.Hauteur_Humidite = max(monArbre->gg.Hauteur_Humidite, arbreTemp->gg.Hauteur_Humidite);
        } else {
            if(monArbre->gg.ID_station > arbreTemp->gg.ID_station){
                monArbre->gauche = insertionHauteur_Humidite(monArbre->gauche, arbreTemp);
            } else {
                monArbre->droit = insertionHauteur_Humidite(monArbre->droit, arbreTemp);
            }
        }
    } else {
        monArbre = arbreTemp;
    }
    return monArbre;
}